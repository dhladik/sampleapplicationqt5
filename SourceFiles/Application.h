#pragma once

#include <QApplication>

#include "Windows/Main.h"

class Application : public QApplication
{
public:
    Application(int &argc, char **argv);
    ~Application();

    int run();

private:
    Windows::Main mMainWindow;
};
