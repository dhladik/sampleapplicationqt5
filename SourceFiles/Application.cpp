#include "Application.h"

Application::Application(int &argc, char **argv)
    : QApplication(argc, argv)
{
}

Application::~Application()
{
}

int Application::run()
{
    mMainWindow.show();

    return QApplication::exec();
}
